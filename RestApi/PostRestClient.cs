﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using VNGraphiteCore.Models;

namespace VNGraphiteCore.RestApi
{
    public class PostRestClient : IPostRestClient
    {
        private readonly RestClient m_client;
        private readonly ILogger _logger;

        public IEnumerable<Post> Posts { get; private set; }
        public IEnumerable<Category> Categories { get; private set; }

        public PostRestClient(ILogger<PostRestClient> logger)
        {
            m_client = new RestClient(Helper.ApiUri);
            m_client.UseJson();
            _logger = logger;
        }

        public List<Post> ListPost()
        {
            try
            {
                _logger.LogInformation($"ListPost: {Helper.ApiUri}");
                var request = new RestRequest("/posts", DataFormat.Json);

                var response = m_client.Get(request);

                //_logger.LogInformation($"ListPost response {JsonConvert.SerializeObject(response)}");

                if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content != null)
                    Posts = JsonConvert.DeserializeObject<IEnumerable<Post>>(response.Content);
            }
            catch (Exception e)
            {
                _logger.LogError($"ListPost Exception: " + e.ToString());
            }

            if (Posts != null)
            {
                return Posts.ToList();
            }

            _logger.LogDebug($"ListPost Posts: {JsonConvert.SerializeObject(Posts)}");

            return new List<Post>();
        }

        public Post GetPost(string slug)
        {
            Post retPost = null;
            try
            {
                var request = new RestRequest($"/posts?Slug={slug}", DataFormat.Json);
                var response = m_client.Get(request);

                _logger.LogInformation($"GetPost {response.StatusCode.ToString()}");

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var jsonData = JsonConvert.DeserializeObject<IEnumerable<Post>>(response.Content);

                    retPost = jsonData.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
            }

            return retPost;
        }

        public IEnumerable<Category> ListCategory()
        {
            try
            {
                var request = new RestRequest("/categories", DataFormat.Json);
                var response = m_client.Get(request);

                _logger.LogInformation($"ListCategory {response.StatusCode.ToString()}");

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    Categories = JsonConvert.DeserializeObject<IEnumerable<Category>>(response.Content);
            }
            catch (Exception e)
            {
                _logger.LogError($"ListCategory Exception " + e.ToString());
            }

            _logger.LogDebug($"ListPost Categories: {JsonConvert.SerializeObject(Categories)}");

            if (Categories != null)
            {
                return Categories.ToList();
            }

            return new List<Category>();
        }

        public List<Post> FilterPost(PostFilter filter)
        {
            try
            {
                string uriFilter = "/posts";

                if (filter.ShowInHomePage != null)
                {
                    uriFilter += $"?ShowHomePage={filter.ShowInHomePage.Value.ToString().ToLower()}";
                }

                if (filter.Publish_Date.Date != DateTime.MinValue.Date)
                {
                    if (uriFilter.Contains('?'))
                    {
                        uriFilter += $"&&published_at={filter.Publish_Date.ToString("yyyy-MM-dd")}";
                    }
                    else
                    {
                        uriFilter += $"?published_at={filter.Publish_Date.ToString("yyyy-MM-dd")}";
                    }
                }

                var request = new RestRequest(uriFilter, DataFormat.Json);
                var response = m_client.Get(request);

                _logger.LogInformation($"FilterPost {response.StatusCode.ToString()}");

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Posts = JsonConvert.DeserializeObject<IEnumerable<Post>>(response.Content);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
            }

            if (Posts != null)
            {
                return Posts.ToList();
            }

            return new List<Post>();
        }

        public List<Post> ListPostByCategory(int categoryId)
        {
            try
            {
                string uriFilter = $"/posts?_where[category.id]={categoryId}";

                var request = new RestRequest(uriFilter, DataFormat.Json);
                var response = m_client.Get(request);

                _logger.LogInformation($"ListPostByCategory {response.StatusCode.ToString()}");

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Posts = JsonConvert.DeserializeObject<IEnumerable<Post>>(response.Content);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
            }

            if (Posts != null)
            {
                return Posts.ToList();
            }

            return new List<Post>();
        }

        public IEnumerable<Image_Galery> GetGalery()
        {
            IEnumerable<Image_Galery> data = new List<Image_Galery>();
            try
            {
                string uriFilter = $"/image-galeries";

                var request = new RestRequest(uriFilter, DataFormat.Json);
                var response = m_client.Get(request);

                _logger.LogInformation($"GetGalery {response.StatusCode.ToString()}");

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    data = JsonConvert.DeserializeObject<IEnumerable<Image_Galery>>(response.Content);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
            }

            if (data != null)
            {
                return data;
            }

            return new List<Image_Galery>();
        }
    }
}
