﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VNGraphiteCore.Models;

namespace VNGraphiteCore.RestApi
{
    public interface IPostRestClient
    {
        List<Post> ListPost();
        List<Post> FilterPost(PostFilter filter);
        List<Post> ListPostByCategory(int categoryId);
        Post GetPost(string slug);
        IEnumerable<Category> ListCategory();
        IEnumerable<Image_Galery> GetGalery();
    }
}
