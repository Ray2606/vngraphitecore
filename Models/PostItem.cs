﻿using System;
using System.Collections.Generic;

namespace VNGraphiteCore.Models
{
    public class PostItem
    {
        public int Id { get; set; }
        public String Content { get; set; }
        public List<PostImage> Images { get; set; }
    }
}
