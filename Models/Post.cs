﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VNGraphiteCore.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        [JsonProperty("category")]
        public Category Category { get; set; }
        public int CategoryId => Category.Id;
        public string CategoryName => Category.Name;
        public string Slug { get; set; }
        public decimal ViewCount { get; set; }
        public PostImage ShortcutImage { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Published_At { get; set; }
        public DateTime Updated_At { get; set; }
        public bool ShowHomePage { get; set; }
        public List<PostItem> PostItems { get; set; }
    }
}
