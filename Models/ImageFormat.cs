﻿using System;
namespace VNGraphiteCore.Models
{
    public class ImageFormat
    {

        public ImageThumb Thumbnail { get; set; }
        public ImageSmall Small { get; set; }
    }
}
