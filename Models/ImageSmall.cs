﻿using System;
namespace VNGraphiteCore.Models
{
    public class ImageSmall
    {
        public string Url { get; set; }
        public string Hash { get; set; }
        public string Name { get; set; }
        public double Size { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
