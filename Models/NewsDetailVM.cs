﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VNGraphiteCore.Models
{
    public class NewsDetailVM
    {
        public Post NewsDetail { get; set; }
        public List<Post> PopularNews { get; set; }
    }
}
