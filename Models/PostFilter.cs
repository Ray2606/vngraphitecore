﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VNGraphiteCore.Models
{
    public class PostFilter
    {
        public DateTime Publish_Date { get; set; }
        public bool? ShowInHomePage { get; set; }
    }
}
