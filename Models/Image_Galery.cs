﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VNGraphiteCore.Models
{
    public class Image_Galery
    {
        public int Id { get; set; }
        public PostImage Image_Content { get; set; }
    }
}
