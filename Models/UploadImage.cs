﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VNGraphiteCore.Models
{
    public class UploadImage
    {
        public string ResourceType { get; set; }
        public ImageFolder CurrentFolder { get; set; }
        public string FileName { get; set; }
        public int Uploaded { get; set; }
    }

    public class ImageFolder {
        public string Path { get; set; }
        public string Url { get; set; }
        public int Acl { get; set; }
    }
}
