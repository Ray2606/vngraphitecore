﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VNGraphiteCore.Models;
using VNGraphiteCore.RestApi;

namespace VNGraphiteCore.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IPostRestClient m_postClient;

        public HomeController(IPostRestClient postClient)
        {
            m_postClient = postClient;
        }

        public IActionResult Index()
        {
            var filterData = m_postClient.FilterPost(new PostFilter
            {
                ShowInHomePage = true
            });

            ViewData["News"] = filterData.OrderByDescending(x => x.Published_At).Where(x => x.ShowHomePage).Take(2).ToList();
            //ViewData["News"] = SampleData.NewsData.OrderByDescending(x => x.PublishDate.Date).Take(2).ToList();

            return View();
        }
    }
}
