﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VNGraphiteCore.Models;
using VNGraphiteCore.Ultilities;

namespace VNGraphiteCore.Controllers
{
    [Route("admin")]
    public class AdminController : BaseController
    {
        private IHostingEnvironment _env;

        public AdminController(IHostingEnvironment env)
        {
            _env = env;
        }


            public IActionResult Index()
        {
            return View();
        }

        [Route("/admin/create")]
        public IActionResult Create() {
            return View();
        }

        [Route("/admin/upload/")]
        [HttpPost]
        public IActionResult UploadImage()
        {
            var file = HttpContext.Request.Form.Files[0];

            var webRoot = _env.WebRootPath;
            var PathWithFolderName = System.IO.Path.Combine(webRoot, "ImageUpload");

            if (!Directory.Exists(PathWithFolderName))
            {
                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(PathWithFolderName);
            }

            if (!System.IO.File.Exists($"{PathWithFolderName}/{file.FileName}"))
            {
                using (var fileStream = new FileStream(Path.Combine(PathWithFolderName, file.FileName), FileMode.Create))
                {
                    file.CopyTo(fileStream);
                }
            }

            var uploadModel = new UploadImage { 
                ResourceType = "Files",
                CurrentFolder = new ImageFolder { 
                    Path = "/",
                    Url = $"{HttpContext.Request.Host}/ImageUpload/",
                    Acl = 255
                },
                FileName = file.FileName,
                Uploaded = 1
            };

            var responseUrl = Json($"{HttpContext.Request.Host}/ImageUpload/{file.FileName}");
            return responseUrl;

            //return Json(JsonConvert.SerializeObject(uploadModel)); 
        }

        [Route("/admin/add-post/")]
        [HttpPost]
        public IActionResult AddPost(IFormFile file)
        {
            var file1 = HttpContext.Request.Form.Files[0];

            //var img = Image.FromStream(formData.OpenReadStream());
            return null;
        }
    }
}
