﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VNGraphiteCore.RestApi;

namespace VNGraphiteCore.Controllers
{
    [Route("hinh-anh")]
    public class GalleryController : BaseController
    {
        private readonly IPostRestClient m_postClient;

        public GalleryController(IPostRestClient postClient)
        {
            m_postClient = postClient;
        }
        public IActionResult Index()
        {
            var galeryData = m_postClient.GetGalery();

            return View(galeryData);
        }
    }
}