﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VNGraphiteCore.Models;
using VNGraphiteCore.RestApi;

namespace VNGraphiteCore.Controllers
{
    [Route("tin-tuc")]
    public class NewsController : BaseController
    {
        private readonly Random rnd = new Random();
        private readonly IPostRestClient m_postClient;

        public NewsController(IPostRestClient postClient)
        {
            m_postClient = postClient;
        }

        public IActionResult Index()
        {
            var categories = m_postClient.ListCategory();
            //var categories = SampleData.CategoryData;
            ViewData["Category"] = categories.ToDictionary(k => k.Id, v => v.Name);
            ViewData["IsMobile"] = RequestExtensions.IsMobileBrowser(Request);

            var data = m_postClient.ListPost();
            //var data = SampleData.NewsData;
            var dictNewsData = data.GroupBy(x => x.CategoryId).ToDictionary(k => k.Key, v => v.OrderByDescending(x => x.Published_At).Take(3).ToList());

            return View(dictNewsData);
        }

        [Route("/tin-tuc/chi-tiet/{slug?}")]
        public IActionResult Detail(string slug = "")
        {
            if (String.IsNullOrEmpty(slug))
            {
                return View("Error");
            }
            //Post newsDetail = SampleData.NewsData.FirstOrDefault(x => x.Slug.Contains(slug));
            var newsDetail = m_postClient.GetPost(slug);

            if (newsDetail == null)
            {
                return View("Error");
            }

            if (newsDetail.PostItems != null && newsDetail.PostItems.Any())
                newsDetail.PostItems = newsDetail.PostItems.OrderBy(x => x.Id).ToList();


            // Giả lập số lượng view
            var data = m_postClient.ListPost();
            //List<Post> data = SampleData.NewsData.ToList();
            foreach (var news in data)
            {
                news.ViewCount = rnd.Next(1, 100000);
            }

            //ViewData["SameData"] = data;
            ViewData["SameData"] = data.Where(x => x.CategoryId == newsDetail.CategoryId).ToList();

            var detailVM = new NewsDetailVM { 
                NewsDetail = newsDetail,
                PopularNews = data.OrderByDescending(x => x.ViewCount).Take(2).ToList()
            };

            return View(detailVM);
        }

        [Route("/tin-tuc/{categoryId}")]
        public IActionResult NewsByCategory(int categoryId)
        {
            var result = m_postClient.ListPostByCategory(categoryId);

            return View(result);
        }
    }
}