﻿import { $, jQuery } from 'jquery';
// export for others scripts to use
window.$ = $;
window.jQuery = jQuery;

import 'popper.js'
import 'bootstrap'
import '@fortawesome/fontawesome-free/js/all'
