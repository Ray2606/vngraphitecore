﻿//import 'jquery'
import HelperMng from './ultils/helper'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
//import 'summernote/dist/summernote-bs4'
//import FroalaEditor from 'froala-editor'
//// Load a plugin.
//import 'froala-editor/js/plugins/align.min.js'

class Admin {
    constructor({ root }) {
        this.$root = $(root);

        this.helper = new HelperMng()
        this.helper.setMobileMenu()
        $('footer').prop('hidden', true)

        //$('#summernote').summernote();

        new FroalaEditor('#editor');

        //ClassicEditor
        //    .create(document.querySelector('#editor'), {
        //        ckfinder: {
        //            uploadUrl: '/admin/upload/',
        //            options: {
        //                resourceType: 'Images'
        //            }
        //        },
        //        image: {
        //            types: ['png', 'jpeg', 'jpg'],
        //        }
        //    })
        //    .then(editor => {
        //        console.log(editor);
        //    })
        //    .catch(error => {
        //        console.error(error);
        //    });
    }

    uploadScanFile(ev) {
        ev.preventDefault();
        const $form = $("#frm", this.$modal);
        const valid = $form.valid();

        if (!valid) {
            return;
        }

        const model = $form.getFormData();

        $.ajax({
            type: "POST",
            data: model,
            contentType: false,
            processData: false,
            url: '/document/upload',
        })
            .done((data) => {
                if (data.success) {
                    this.alert.showSuccess("Upload thành công!")
                    this.$modal.modal('hide');
                }
                else {
                    this.alert.showError(data.message)
                }
            })
            .fail((err) => {
                this.alert.showSystemError(err);
            });
    }
}

const adminMng = new Admin({
    root: '#main'
});