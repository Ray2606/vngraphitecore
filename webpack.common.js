﻿const path = require('path');
const webpack = require("webpack");

module.exports = {
    entry: {
        'index': ['./src/index.js', './src/sass/index.scss'],
        'card-gallery': ['./src/css/cards-gallery.css'],
        'home-mng': ['./src/home.js'],
        'general-info': ['./src/general-info.js'],
        'team-mng': ['./src/team.js'],
        'product-mng': ['./src/product.js'],
        'partner-mng': ['./src/partner.js'],
        'news-mng': ['./src/news-mng.js'],
        'contact-mng': ['./src/contact.js'],
        'admin': ['./src/admin.js'],
        'admin-lte': ['./src/admin-lte/js/adminlte.js', './src/admin-lte/css/adminlte.css'],
        //'summernote': ['./src/components/js/summernote/', './src/admin-lte/css/adminlte.css'],
    },
    output: {
        filename: '[name].bundle.js',
        chunkFilename: '[name].bundle.js',
        //production
        //filename: '[contenthash].js',
        //chunkFilename: '[contenthash].js',
        path: path.resolve(__dirname, 'wwwroot/dist'),
        publicPath: "/dist/"
    },
    z
    module: {
        rules: [
            {
                test: /\.(scss|sass|css)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].bundle.css'
                            // production
                            //name: '[contenthash].css'
                        }
                    },
                    {
                        loader: 'extract-loader'
                    },
                    {
                        loader: "css-loader",
                        //options: {
                        //    minimize: true || {/* or CSSNano Options see http://cssnano.co/guides/ */ }
                        //}
                    },
                    {
                        loader: "sass-loader"
                    }
                ]
            }
        ]
    }
};